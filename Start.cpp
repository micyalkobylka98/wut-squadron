#include "Game.h"
#include "Start.h"
#include "ui_Start.h"
#include <windows.h>
#include <QApplication>
#include <QGraphicsScene>
#include <QPixmap>

extern Game * game;

Start::Start(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Start)
{
    ui->setupUi(this);
    QPixmap pix(":/images/Start_BG_small.png");     //dodanie tla ekranu startowego
    ui->menu_bg->setPixmap(pix);
}

Start::~Start(){
    delete ui;
}

void Start::on_New_game_clicked(){                  //uruchomienie nowej gry
    this->close();
    ui->~Start();
    game = new Game();
    game->show();
    game->start();
}

void Start::on_Info_clicked(){                      //wyswietlenie okienka info
    Info * info = new Info();
    info->show();
}

void Start::on_Quit_clicked(){                      //zamkniecie calego programu
    ui->~Start();
    exit(1);
}




