#include "Score.h"
#include <QFont>

Score::Score(QGraphicsItem *parent): QGraphicsTextItem(parent){
    score=0;

    setPlainText("HIT POINTS: "+QString::number(score));     //pierwsze wyświetlenie punktow oraz ustalenie parametrow tekstu
    setDefaultTextColor(Qt::black);
    setFont(QFont("ISOCP",16));
}

void Score::increase(){
    score++;
    setPlainText("HIT POINTS: "+QString::number(score));     //dodawanie punkty
}
void Score::increase_token(){                                //dodawanie punktow za bonus/token
    score=score+2;
    setPlainText("HIT POINTS: "+QString::number(score));
}

int Score::getScore(){                                       //sprawdzenie wyniku
    return score;
}
