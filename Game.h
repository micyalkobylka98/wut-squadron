#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QMediaPlaylist>
#include "Player.h"
#include "Score.h"
#include "Health.h"
#include "Token_points.h"

class Game: public QGraphicsView{
public:
    Game();
    ~Game();

    void start();
    void endgame();

    QGraphicsScene * scene;
    Player * player;
    Score * score;
    Health * health;
    QMediaPlaylist * playlist;
    QMediaPlayer * music;

};

#endif // GAME_H
