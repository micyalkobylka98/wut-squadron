#include "Game.h"
#include "Player.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Token_points.h"
#include "Token_HP.h"
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QDebug>
#include <QTimer>
#include <windows.h>

extern Game * game;

Player::Player(QGraphicsItem *parent): QGraphicsPixmapItem(parent){
    //ustalenie parametrów dzwięku wystrzału
    bulletsound = new QMediaPlayer();
    bulletsound->setMedia(QUrl("qrc:/sounds/LMG_shoot_3.mp3"));

    //grafika samolotu gracza
    setPixmap(QPixmap(":/images/gracz.png"));

    //timer odpowiadajacy za ruch gracza
    QTimer * timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));
    timer->start(50);
    //timer odpowiedzialny za opoznianie kolejnych wystrzałów
    timerA = new QTimer();
    connect(timerA, SIGNAL(timeout()), this, SLOT(reload()));

}

void Player::keyPressEvent(QKeyEvent *event){                      //setowanie wcisniecia klawiszy
    if (event->key() == Qt::Key_Left){
        leftPressed = true;
    }
    else if (event->key() == Qt::Key_Right){
       rightPressed = true;
    }
    if (event->key() == Qt::Key_Up){
        upPressed = true;
    }
    else if (event->key() == Qt::Key_Down){
        downPressed = true;
    }

    else if (event->key() == Qt::Key_Space && reloaded == true){  //strzelanie spacja
        //Po wciśnięciu spacji wystrzel pocisk
        //z zachowaniem odstępu czasu między strzałami
        timerA->start(200);
        reloaded = false;
        Bullet *bullet = new Bullet();
        bullet->setPos(x()+45,y()-6);
        scene()->addItem(bullet);

        if(bulletsound->state() == QMediaPlayer::PlayingState){     //odpalanie dzwieku strzalu
            bulletsound->setPosition(0);
        }
        else if (bulletsound->state() == QMediaPlayer::StoppedState){
            bulletsound->play();
        }
    }
}

void Player::keyReleaseEvent(QKeyEvent *event){         //resetowanie przcisniecia klawiszy
    if (event->key() == Qt::Key_Left){
        leftPressed = false;
    }
    else if (event->key() == Qt::Key_Right){
        rightPressed = false;
    }
    if (event->key() == Qt::Key_Up){
        upPressed = false;
    }
    else if (event->key() == Qt::Key_Down){
        downPressed = false;
    }
}


void Player::spawn_enemy(){
    Enemy *enemy = new Enemy();
    scene()->addItem(enemy);
}

void Player::spawn_token_points(){
    Token_points *token_points = new Token_points();
    scene()->addItem(token_points);
}

void Player::spawn_token_HP(){
    Token_HP *token_HP = new Token_HP();
    scene()->addItem(token_HP);
}

//funkcje odpowiadajace za ruch gracza
void Player::move(){
    if (leftPressed){
        if (pos().x() > 0){
            setPos(x()-15,y());
        }
    }

    if(rightPressed){
        if (pos().x()+100 < 1200){
            setPos(x()+15,y());
        }
    }

    if (upPressed){
        if (pos().y() > 600){
            setPos(x(),y()-10);
        }
    }

    if(downPressed){
        if (pos().y() < 700){
            setPos(x(),y()+10);
        }
    }

    //przerwanie gre, gdy skoncza sie zycia
    if(game->health->getHealth()==0){
        game->endgame();
    }

}
//wymuszenie odstepu miedzy wystrzalami
void Player::reload(){
    timerA->stop();
    reloaded = true;
}




