#include <QApplication>
#include "Game.h"
#include "Start.h"

Game * game;

int main(int argc, char *argv[]){
    QApplication a(argc, argv);
    Start start;
    start.show();

    return a.exec();
}
