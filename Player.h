#ifndef PLAYER_H
#define PLAYER_H

#include <Enemy.h>
//#include <QGraphicsPixmapItem>
//#include <QObject>
//#include <QGraphicsItem>
#include <QMediaPlayer>
#include <QTimer>

class Player: public QObject,public QGraphicsPixmapItem{
    Q_OBJECT
public:
    Player(QGraphicsItem * parent=0);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent (QKeyEvent * event);
    Enemy * enemy;

public slots:
    void spawn_enemy();
    void spawn_token_points();
    void spawn_token_HP();

private:
    QMediaPlayer * bulletsound;
    bool reloaded = true;
    bool leftPressed = false;
    bool rightPressed = false;
    bool upPressed = false;
    bool downPressed = false;
    QTimer * timerA = nullptr;

private slots:
    void move();
    void reload();

};

#endif // PLAYER_H
