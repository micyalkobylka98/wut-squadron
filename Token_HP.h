#ifndef TOKEN_HP_H
#define TOKEN_HP_H

#include "Bonus.h"

class Token_HP: public Bonus, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    Token_HP();
    ~Token_HP();
public slots:
    void remove();
};



#endif // TOKEN_HP_H
