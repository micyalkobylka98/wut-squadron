#include "Enemy.h"
#include "Game.h"
#include "Health.h"
#include <QTimer>
#include <QList>
#include <stdlib.h>

extern Game *game;

Enemy::Enemy(QGraphicsItem *parent):QObject(), QGraphicsPixmapItem(parent) {

    int random_number =rand() % 1050;  //dodawanie wrogów na ekran oraz przemieszczanie ich
    setPos(random_number,-100);
    setPixmap(QPixmap(":/images/wrog.png"));

    //setTransformOriginPoint(70,50);   //przesuniecie srodka grafiki
    //setRotation(180);                 //obrócenie grafiki

    QTimer *timer =  new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(40);
}

void Enemy::move(){

    setPos(x(),y()+4); // przesuwa przeciwnika w dol

    QList<QGraphicsItem *> collision = collidingItems();            //sprawdza czy przeciwnik nie uderzyl gracza
    for(int i = 0; i < collision.size(); i++){
        if(typeid (*(collision[i])) == typeid(Player)){
            game->health->decrease();
            scene()->removeItem(this);
            delete this;
        }
    }

    if(pos().y()-100 > 700){                    //odejmuje zycie gdy wrog przekroczy dolna krawedz
        game->health->decrease();
        scene()->removeItem(this);
        delete this;
    }
}
