#include "Bullet.h"
#include <QTimer>
#include <QDebug>
#include <QGraphicsScene>
#include <QList>
#include "Enemy.h"
#include "Game.h"
#include "Token_points.h"
#include "Token_HP.h"
#include <typeinfo>

extern Game *game;

Bullet::Bullet(QGraphicsItem * parent): QObject(), QGraphicsPixmapItem(parent) {

    setPixmap(QPixmap(":/images/pocisk2.png"));

    //ruch pocisku powiazany z funkcja czasu
    QTimer *timer =  new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(30);
}

void Bullet::move(){

    QList<QGraphicsItem *> colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; ++i){
        if (typeid(*(colliding_items[i])) == typeid(Enemy)){                    //sprawdaz czy pocisk dosiegl wroga
            //dodaj punkty
            game->score->increase();
            //usun oba
            scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);
            delete colliding_items[i];
            delete this;
            return;
        }
        else if (typeid(*(colliding_items[i])) == typeid(Token_points)){       //sprawdza czy pocisk trafil w token
            //dodaj punkty
            game->score->increase_token();
            //usun oba
            scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);
            delete colliding_items[i];
            delete this;
            return;
        }
        else if (typeid(*(colliding_items[i])) == typeid(Token_HP)){            //sprawdza czy pocisk trafil w token_HP
            //dodaj punkty
            game->health->increase();
            //usun oba
            scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);
            delete colliding_items[i];
            delete this;
            return;
        }
    }

    setPos(x(),y()-10);                 //jeśli w nic nie trafił to pocisk leci dalej

    if(pos().y()+100 < 0){              //usuwa pocisk po dotarciu do gornej krawedzi sceny
        scene()->removeItem(this);
        delete this;

    }
}
