#ifndef TOKEN_POINTS_H
#define TOKEN_POINTS_H

#include "Bonus.h"

class Token_points: public Bonus, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    Token_points();
    ~Token_points();
public slots:
    void remove();

};

#endif // TOKEN_POINTS_H
