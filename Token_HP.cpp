#include "Token_HP.h"
#include "Game.h"
#include <QTimer>
#include <QDebug>
#include <QGraphicsScene>
#include <QList>
#include <stdlib.h>

extern Game *game;

Token_HP::Token_HP():QGraphicsPixmapItem(){
    int random_number_x = rand() % 1150;                    //spawnowanie tokena w losowych wspolrzednych
    int random_number_y = rand() % 300;
    setPos(random_number_x,random_number_y);

    setPixmap(QPixmap(":/images/token_HP.png"));            //dodanie ikony tokena

    QTimer *timer =  new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(remove()));   //usuniecie tokena po kilku sekundach

    timer->start(5500);
}

Token_HP::~Token_HP(){

}

void Token_HP::remove(){
    scene()->removeItem(this);
    delete this;
}




