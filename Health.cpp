#include "Health.h"
#include <QFont>

Health::Health(QGraphicsItem *parent): QGraphicsTextItem(parent){
    health=3;
    setPlainText(QString("HEALTH: ")+QString::number(health));          //pierwsze wyświetlenie wytrzymalosci oraz ustalenie parametrow tekstu
    setDefaultTextColor(Qt::red);
    setFont(QFont("ISOCP",16));
}

void Health::decrease(){                                                //zmniejsz wytrzymalosc gracza
    health--;
    setPlainText(QString("HEALTH: ")+QString::number(health));
}

void Health::increase(){                                                //warunkowe zwiekszenie wytrzymalosc gracza
    if(health < 3){
    health++;
    setPlainText("HEALTH: "+QString::number(health));
    }
}


int Health::getHealth(){                                                //sprawdzanie wartosci HP
    return health;
}

