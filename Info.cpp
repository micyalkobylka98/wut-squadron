#include "Info.h"
#include "ui_Info.h"
#include <QPixmap>

Info::Info(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Info){

    ui->setupUi(this);
    QPixmap pix(":/images/szablon_info.png");       //dodanie grafiki tla
    ui->info_bg->setPixmap(pix);
}
Info::~Info(){
    delete ui;
}


void Info::on_back_clicked(){                       //zamkniecie okienka info
    ui->~Info();
    this->close();
}

