#ifndef GAME_OVER_H
#define GAME_OVER_H

#include <QWidget>

namespace Ui {
class Game_over;
}
class Game_over:public QWidget{
    Q_OBJECT
public:
    explicit Game_over(QWidget *parent = nullptr);
    ~Game_over();


private slots:
    void on_Again_clicked();
    void on_Quit_clicked();

private:
    Ui::Game_over *ui;
};

#endif // GAME_OVER_H
