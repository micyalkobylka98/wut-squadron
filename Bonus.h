#ifndef BONUS_H
#define BONUS_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>

class Bonus: public QObject{

public slots:
    virtual void remove();
};

#endif // BONUS_H
