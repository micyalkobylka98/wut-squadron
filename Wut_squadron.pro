#-------------------------------------------------
#
# Project created by QtCreator 2021-05-05T19:24:45
#
#-------------------------------------------------

QT       += core gui \
         multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = Wut_squadron
TEMPLATE = app


SOURCES += main.cpp \
    Bonus.cpp \
    Bullet.cpp \
    Enemy.cpp \
    Game.cpp \
    Game_over.cpp \
    Info.cpp \
    Score.cpp \
    Player.cpp \
    Health.cpp \
    Start.cpp \
    Token_HP.cpp \
    Token_points.cpp

HEADERS  += \
    Bonus.h \
    Bullet.h \
    Enemy.h \
    Game.h \
    Game_over.h \
    Info.h \
    Score.h \
    Player.h \
    Health.h \
    Start.h \
    Token_HP.h \
    Token_points.h

RESOURCES += \
    res.qrc

FORMS += \
    Game_over.ui \
    Info.ui \
    Start.ui
