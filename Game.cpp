#include "Player.h"
#include "Game.h"
#include "Game_over.h"
#include <QApplication>
#include <QTimer>
#include <QRandomGenerator>
#include <QImage>
#include <QDebug>
#include <stdlib.h>
#include <QMovie>



extern Game * game;

Game::Game(){

    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,1200,800);

    setBackgroundBrush(QBrush(QImage(":/images/sky2.png")));

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1200,800);
}

Game::~Game()
{

}

void Game::start(){                 //Funkcja odpowiedzialna za uruchomienie gry

    player = new Player();          //Tworzenie gracza oraz mini interfejsu
    player->setPos(550,710);
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();
    scene->addItem(player);

    score = new Score();            //dodanie wyniku na ekran
    score->setPos(0,scene->height()-65);
    scene->addItem(score);

    health = new Health();          //dodanie wytrzymalości na ekran
    health->setPos(0,scene->height()-35);
    scene->addItem(health);

    //Generowanie przeciwników
    //int random_number_enemy = (rand() % 5000 + 5000) ;
    int spawn_en = QRandomGenerator::global()->bounded(4000,5000);
    QTimer * timer = new QTimer();
    QObject::connect(timer,SIGNAL(timeout()),player,SLOT(spawn_enemy()));
    timer->start(spawn_en);
    //qDebug()/*<<"random_number_enemy: "*/<<spawn_en;


    //Generowanie tokena Points
    int random_number_token_points = (19000 + (rand() % 6000)) ;
    QTimer * timer_token_points = new QTimer();
    QObject::connect(timer_token_points,SIGNAL(timeout()),player,SLOT(spawn_token_points()));
    timer_token_points->start(random_number_token_points);
    //qDebug()/*<<"random_number_enemy: "*/<<random_number_token_points;

    //Generowanie tokena HP
    int random_number_token_HP = (32000 + (rand() % 10000)) ;
    QTimer * timer_token_HP = new QTimer();
    QObject::connect(timer_token_HP,SIGNAL(timeout()),player,SLOT(spawn_token_HP()));
    timer_token_HP->start(random_number_token_HP);
    //qDebug()/*<<"random_number_enemy: "*/<<random_number_token_HP;

    //Ścieżka dzwiękowa w tle
    /*QMediaPlaylist * */playlist = new QMediaPlaylist();
    playlist->addMedia(QUrl("qrc:/sounds/war_music.mp3"));
    playlist->addMedia(QUrl("qrc:/sounds/Waterflame - Glorious morning.mp3"));
    playlist->setPlaybackMode(QMediaPlaylist::Loop);

    /*QMediaPlayer * */music = new QMediaPlayer();
    music->setPlaylist(playlist);
    music->play();

}

void Game::endgame(){                   //Usuwanie biezacej gry
    QList <QGraphicsItem*> all = items();
    for (int i = 0; i < all.size(); i++){
        //if(typeid (*(all[i])) != typeid (Player)){
        delete all[i];
        //}
    }
    Game_over * game_over = new Game_over;
    game_over->show();
    music->stop();
    delete game;





}
