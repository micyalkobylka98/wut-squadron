#include "Game_over.h"
#include "ui_Game_over.h"
#include "Game.h"
#include "Start.h"
#include "Score.h"
#include <QPixmap>



extern Game * game;

Game_over::Game_over(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Game_over){

    ui->setupUi(this);
    QPixmap pix(":/images/game_over_bg.png");           //dodanie grafiki tla
    ui->go_bg->setPixmap(pix);

    int points = game->score->getScore();               //wyswietlenie wyniku na koncu gry
    ui->pts->setNum(points);
}

Game_over::~Game_over(){
    delete ui;
}

void Game_over::on_Again_clicked(){                     //uruchomienie nowej gry
    this->close();
    ui->~Game_over();
    game = new Game();
    game->show();
    game->start();
}


void Game_over::on_Quit_clicked(){                      //zamknięcie gry
    ui->~Game_over();
    exit(1);
}

