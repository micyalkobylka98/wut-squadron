#ifndef START_H
#define START_H

#include <QWidget>
#include <QMainWindow>
#include "Info.h"
#include "Game_over.h"

class Info;

namespace Ui {
class Start;
}
class Start : public QWidget{
    Q_OBJECT
public:
    explicit Start(QWidget *parent = nullptr);
    ~Start();
private slots:
    void on_New_game_clicked();
    void on_Info_clicked();
    void on_Quit_clicked();


private:
    Ui::Start *ui;
};



#endif // START_H
